from typing import Optional

import fastapi
from fastapi import Depends
from pydantic import BaseModel
from app import schemas, models
from app.api import deps

router = fastapi.APIRouter()


class Item(BaseModel):
    name: str
    price: float
    is_offer: Optional[bool] = None


@router.get("/", response_model=schemas.Token)
def read_root():


    return {"Hello": "World"}


@router.get("/items/{item_id}")
def read_item(item_id: int, q: Optional[str] = None):
    return {"item_id": item_id, "q": q}


@router.put("/items/{item_id}")
def update_item(item_id: int, item: Item, current_user: models.User = Depends(deps.get_current_user)):
    return {"item_name": item.name, "item_id": item_id}
