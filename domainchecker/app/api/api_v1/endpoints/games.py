from typing import Any, List

from fastapi import APIRouter, Depends, HTTPException, status

from sqlalchemy.orm import Session

from app import crud, models, schemas
from app.api import deps
from app.models import Card, Game
from app.schemas import NewGame, GameUpdate, ResumeGame, GameGetValue
from app.utils import generate_random_numbers

router = APIRouter()


@router.post("/new-game", response_model=schemas.NewGame)
def post_new_game(
        *,
        db: Session = Depends(deps.get_db),
        current_user: models.User = Depends(deps.get_current_active_superuser)
) -> Any:
    """
    Create new user.
    """
    numbers_str = generate_random_numbers()

    games = crud.game.get_multi_by_owner(
        db=db,
        owner_id=current_user.id
    )

    games_resume = list(filter(lambda x: x.state < 20, games))

    if games_resume:

        for game in games_resume:
            game_item = crud.game.get(db, game.id)
            game_in = GameUpdate(
                state=40
            )
            crud.game.update(
                db=db,
                user=current_user,
                db_obj=game_item,
                obj_in=game_in
            )

    game = crud.game.create(db, current_user, numbers_str)

    my_best_score = 0
    games_best_score = crud.game.get_best_score(db=db, owner_id=current_user.id, limit=1)
    if games_best_score:
        my_best_score = games_best_score[0].click_amount

    new_game = NewGame(
        index_first=None,
        index_second=None,
        state=game.state,
        my_best_score=my_best_score,
        id=game.id,
        global_best=crud.game.get_global_best(db)
    )
    return new_game


@router.post("/resume-game", response_model=schemas.ResumeGame, status_code=status.HTTP_201_CREATED)
def post_resume_game(
        *,
        db: Session = Depends(deps.get_db),
        current_user: models.User = Depends(deps.get_current_active_superuser),
) -> Any:
    games = crud.game.get_multi_by_owner(
        db=db,
        owner_id=current_user.id
    )

    games_resume = list(filter(lambda x: x.state < 20, games))

    if len(games_resume) > 1:

        for game in games_resume:
            game_item = crud.game.get(db, game.id)
            game_in = GameUpdate(
                state=40
            )
            crud.game.update(
                db=db,
                user=current_user,
                db_obj=game_item,
                obj_in=game_in
            )

        raise HTTPException(
            status_code=400,
            detail="Can't Get Resume game something went wrong",
        )

    elif len(games_resume) != 0:
        game_item = games_resume[0]

        resume_game = ResumeGame(
            index_first=game_item.index_first,
            index_second=game_item.index_second,
            state=game_item.state,
            id=game_item.id
        )

        games_best_score = crud.game.get_best_score(db=db, owner_id=current_user.id, limit=1)
        if games_best_score:
            resume_game.my_best_score = games_best_score[0].click_amount

        resume_game.click_amount = game_item.click_amount
        resume_game.cards = {
            card.index_number: card.card_no for card in game_item.cards
        }
        resume_game.global_best = crud.game.get_global_best(db)

        return resume_game

    else:
        raise HTTPException(
            status_code=400,
            detail="Don't Have Game To Resume",
        )


@router.post("/get-card-number", response_model=schemas.ResumeGame)
def get_value_from_index(
        *,
        db: Session = Depends(deps.get_db),
        game_update: schemas.GameGetValue,
        current_user: models.User = Depends(deps.get_current_active_superuser),
) -> Any:
    """
    Get Card Number.
    """
    game_id = game_update.id
    index = game_update.index
    game_item = crud.game.get(db, game_id)

    if not 0 < index < 13:
        raise HTTPException(
            status_code=400,
            detail="index out of range",
        )

    if game_item.owner_id != current_user.id:
        raise HTTPException(
            status_code=400,
            detail="Can't Select Card From Game Of Another Owner",
        )

    if game_item.state > 10:
        raise HTTPException(
            status_code=400,
            detail="Game Id {} Is End".format(game_id),
        )
    
    resume_game = ResumeGame(
        state=game_item.state,
        id=game_item.id
    )
    games_best_score = crud.game.get_best_score(db=db, owner_id=current_user.id, limit=1)
    if games_best_score:
        resume_game.my_best_score = games_best_score[0].click_amount

    number_list_str = game_item.random_numbers.split(",")
    number_list = [int(number_str) for number_str in number_list_str]

    resume_game.cards = {
        card.index_number: card.card_no for card in game_item.cards
    }

    if resume_game.cards.get(index):
        games_best_score = crud.game.get_best_score(db=db, owner_id=current_user.id, limit=1)
        if games_best_score:
            resume_game.my_best_score = games_best_score[0].click_amount
        resume_game.click_amount = game_item.click_amount
    elif not game_item.index_first:
        create_card_index(db, game_item, index, number_list)
        game_in = GameUpdate(
            index_first=index,
            state=10,
            click_amount=(game_item.click_amount + 1)
        )
        game_item = crud.game.update(
            db=db,
            user=current_user,
            db_obj=game_item,
            obj_in=game_in
        )

        resume_game.click_amount = game_item.click_amount
        resume_game.cards = {
           card.index_number: card.card_no for card in game_item.cards
        }

    elif index == game_item.index_first:
        games_best_score = crud.game.get_best_score(db=db, owner_id=current_user.id, limit=1)
        if games_best_score:
            resume_game.my_best_score = games_best_score[0].click_amount
        resume_game.click_amount = game_item.click_amount
        
    elif number_list[game_item.index_first - 1] == number_list[index-1]:

        create_card_index(db, game_item, index, number_list)

        game_in = GameUpdate(
            index_first=None,
            click_amount=(game_item.click_amount + 1)
        )

        game_item = crud.game.update(
            db=db,
            user=current_user,
            db_obj=game_item,
            obj_in=game_in
        )
        games_best_score = crud.game.get_best_score(db=db, owner_id=current_user.id, limit=1)
        if games_best_score:
            resume_game.my_best_score = games_best_score[0].click_amount
        resume_game.click_amount = game_item.click_amount
        resume_game.cards = {
            card.index_number: card.card_no for card in game_item.cards
        }
    elif number_list[game_item.index_first - 1] != number_list[index - 1]:
        cards = {
            card.index_number: card.card_no for card in game_item.cards
        }
        cards[index] = number_list[index - 1]
        delete_card_index(db, game_item, game_item.index_first)
        game_in = GameUpdate(
            index_first=None,
            click_amount=(game_item.click_amount + 1)
        )
        game_item = crud.game.update(
            db=db,
            user=current_user,
            db_obj=game_item,
            obj_in=game_in
        )

        games_best_score = crud.game.get_best_score(db=db, owner_id=current_user.id, limit=1)
        if games_best_score:
            resume_game.my_best_score = games_best_score[0].click_amount

        resume_game.click_amount = game_item.click_amount
        resume_game.cards = cards

    if len(resume_game.cards) == 12:
        game_in = GameUpdate(
            state=20
        )
        game_item = crud.game.update(
            db=db,
            user=current_user,
            db_obj=game_item,
            obj_in=game_in
        )
        resume_game.state = game_item.state

    resume_game.global_best = crud.game.get_global_best(db)

    return resume_game


def create_card_index(db: Session, game_item: Game, index: int, number_list: List[int]) -> None:
    card_in = Card(
        game_id=game_item.id,
        index_number=index,
        card_no=number_list[index - 1]
    )
    crud.card.create(
        db=db,
        card_in=card_in
    )

    
def delete_card_index(db: Session, game_item: Game, index: int) -> None:
    crud.card.remove_by_index_and_game(
        db=db,
        game_id=game_item.id,
        index=index
    )


