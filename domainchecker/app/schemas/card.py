from typing import Optional

from pydantic import BaseModel, EmailStr


# Shared properties
class CardBase(BaseModel):
    game_id: Optional[int] = None
    index_number: Optional[int] = None
    card_no: Optional[int] = None


# # Properties to receive via API on creation
class CardCreate(CardBase):
    pass


# # Properties to receive via API on update
class CardUpdate(CardBase):
    pass


class CardInDBBase(CardBase):
    id: Optional[int] = None

    class Config:
        orm_mode = True


# # Additional properties to return via API
class Card(CardInDBBase):
    pass


# Properties properties stored in DB
class CardInDB(CardInDBBase):
    pass
