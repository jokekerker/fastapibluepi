from typing import Optional

from pydantic import BaseModel, EmailStr


# Shared properties
class GameBase(BaseModel):
    index_first: Optional[int] = None
    state: Optional[int] = 0
    click_amount: Optional[int] = 0
    my_best_score: Optional[int] = 0
    global_best: Optional[int] = 0


# # Properties to receive via API on creation
class GameGetValue(GameBase):
    id: int
    index: int


# # Properties to receive via API on creation
class GameCreate(GameBase):
    random_numbers: str


class NewGame(GameBase):
    id: Optional[int] = None


class ResumeGame(GameBase):
    id: Optional[int] = None
    cards: Optional[dict] = None


# # Properties to receive via API on update
class GameUpdate(GameBase):
    state: Optional[int] = None


class GameInDBBase(GameBase):
    id: Optional[int] = None
    owner_id: Optional[int] = None

    class Config:
        orm_mode = True


# # Additional properties to return via API
class Game(GameInDBBase):
    pass


# Properties properties stored in DB
class GameInDB(GameInDBBase):
    pass
