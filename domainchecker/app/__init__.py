# -*- coding: utf-8 -*-
from app.api.api_v1.endpoints import health

__all__ = ["health"]