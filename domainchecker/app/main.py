# main.py

import uvicorn
import fastapi

from app.api.api_v1.endpoints import route
from app.api.api_v1.api import api_router
from app.core.config import settings
from app.db.init_db import init_db
from app.db.session import SessionLocal, engine, Base
from starlette.middleware.cors import CORSMiddleware

Base.metadata.create_all(bind=engine)
app = fastapi.FastAPI(
    title=settings.PROJECT_NAME, openapi_url=f"{settings.API_V1_STR}/openapi.json"
)


def configure():
    app.include_router(route.router, prefix=settings.API_V1_STR)
    app.include_router(api_router, prefix=settings.API_V1_STR)


if settings.BACKEND_CORS_ORIGINS:
    app.add_middleware(
        CORSMiddleware,
        allow_origins=[str(origin) for origin in settings.BACKEND_CORS_ORIGINS],
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )


configure()
db = SessionLocal()
init_db(db)
# app.add_middleware(PrometheusMiddleware)

if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8080)
