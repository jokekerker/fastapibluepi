from random import randrange
from typing import List, Union, Dict, Any

from fastapi.encoders import jsonable_encoder
from sqlalchemy.orm import Session

from app.crud.base import CRUDBase
from app.models import Card
from app.schemas.card import CardCreate, CardUpdate


class CRUDItem(CRUDBase[Card, CardCreate, CardUpdate]):

    def create(self, db: Session, card_in: Card) -> CardCreate:
        db_obj = card_in
        db.add(db_obj)
        db.commit()
        db.refresh(db_obj)
        return db_obj

    def remove_by_index_and_game(self, db: Session, game_id: int, index: int) -> List[Card]:
        obj_list = db.query(self.model).filter(
            Card.game_id == game_id,
            Card.index_number == index
        ).all()
        
        if obj_list:
            for obj_delete in obj_list:
                db.delete(obj_delete)
                db.commit()
            
        return obj_list


card = CRUDItem(Card)
