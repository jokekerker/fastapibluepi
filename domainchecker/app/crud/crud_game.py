from random import randrange
from typing import List, Union, Dict, Any

from fastapi.encoders import jsonable_encoder
from sqlalchemy.orm import Session

from app.crud.base import CRUDBase
from app.models import Game, User
from app.schemas.game import GameCreate, GameUpdate, NewGame, ResumeGame


class CRUDItem(CRUDBase[Game, GameCreate, GameUpdate]):

    def create(self, db: Session, user: User, numbers_str: str) -> NewGame:
        db_obj = Game(
            random_numbers=numbers_str,
            owner_id=user.id
        )
        db.add(db_obj)
        db.commit()
        db.refresh(db_obj)
        return db_obj

    def update(
        self, db: Session, *, user: User, db_obj: Game, obj_in: Union[GameUpdate, Dict[str, Any]]
    ) -> Game:
        if isinstance(obj_in, dict):
            update_data = obj_in
        else:
            update_data = obj_in.dict(exclude_unset=True)

        return super().update(db, db_obj=db_obj, obj_in=update_data)

    def create_with_owner(
        self, db: Session, *, obj_in: GameCreate, owner_id: int
    ) -> Game:
        obj_in_data = jsonable_encoder(obj_in)
        db_obj = self.model(**obj_in_data, owner_id=owner_id)
        db.add(db_obj)
        db.commit()
        db.refresh(db_obj)
        return db_obj

    def get_multi_by_owner(
        self, db: Session, *, owner_id: int, skip: int = 0, limit: int = 100
    ) -> List[Game]:
        return (
            db.query(self.model)
            .filter(Game.owner_id == owner_id)
            .offset(skip)
            .limit(limit)
            .all()
        )

    def get_best_score(
        self, db: Session, *, owner_id: int, skip: int = 0, limit: int = 100
    ) -> List[Game]:
        obj_list = db.query(self.model).filter(
            Game.owner_id == owner_id,
            Game.state == 20
        ).order_by(
            self.model.click_amount.asc()
        ).limit(
            limit
        ).all()
        return obj_list

    def get_global_best(
            self, db: Session
    ) -> int:
        global_best_score = 0
        obj_list = db.query(self.model).filter(
            Game.state == 20
        ).order_by(
            self.model.click_amount.asc()
        ).limit(
            1
        ).all()

        if obj_list:
            global_best_score = obj_list[0].click_amount

        return global_best_score


game = CRUDItem(Game)
