from sqlalchemy import Column, ForeignKey, Integer, String
from sqlalchemy.orm import relationship

from app.db.session import Base


class Game(Base):
    __tablename__ = "games"
    __table_args__ = {'extend_existing': True}

    id = Column(Integer, primary_key=True, index=True)

    random_numbers = Column(String, index=True)
    click_amount = Column(Integer, default=0)
    index_first = Column(Integer, default=None)
    index_second = Column(Integer, default=None)
    # 0 = New Game / 10 = Selected First / 20 = Complete Select / 40 = Cancel
    state = Column(Integer, default=0)

    owner_id = Column(Integer, ForeignKey("users.id"))
    owner = relationship("User", back_populates="games")
    cards = relationship("Card", back_populates="game")


class Card(Base):
    __tablename__ = "cards"
    __table_args__ = {'extend_existing': True}

    id = Column(Integer, primary_key=True, index=True)
    game_id = Column(Integer, ForeignKey("games.id"))
    index_number = Column(Integer, default=None)
    card_no = Column(Integer, default=None)
    game = relationship("Game", back_populates="cards")
