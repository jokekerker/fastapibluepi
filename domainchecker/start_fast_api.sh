export PYTHONUNBUFFERED=1
export SQLALCHEMY_DATABASE_URI=postgresql://odoo:odoo@localhost/FastApiTest1
export PROJECT_NAME=TestFatApi
export SERVER_NAME=localhost
export SERVER_HOST=http://localhost
export SENTRY_DSN=
export POSTGRES_SERVER=
export POSTGRES_USER=
export POSTGRES_PASSWORD=
export POSTGRES_DB=
export FIRST_SUPERUSER=admin@localhost.com
export FIRST_SUPERUSER_PASSWORD=p@ssw0rd

uvicorn app.main:app --reload